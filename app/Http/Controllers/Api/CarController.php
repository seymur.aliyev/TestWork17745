<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SellCarRequest;
use App\Models\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $query = Car::query();

        if (auth()->user()->type==0) {
            $query->where('user_id', auth()->user()->id);
        }

        $cars = $query->with('user:id,name')->paginate(3);

        return response()->json($cars, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SellCarRequest $request)
    {
        $request->validated();

        Car::create([
            'make' => $request->make,
            'model' => $request->model,
            'user_id' => auth()->id(),
            'price' => $request->price,
            'year' => $request->year
        ]);

        return response()->json(['message' => 'messages.successfully_created'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Car $car)
    {
        return response()->json($car, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SellCarRequest $request, Car $car)
    {
        $request->validated();

        $car->update([
            'make' => $request->make,
            'model' => $request->model,
            'year' => $request->year,
            'price' => $request->price
        ]);

        return response()->json(['message' => __('messages.successfully_updated')], 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Car $car)
    {
        $car->delete();
        return response()->json(['message' => 'messages.successfully_deleted'], 200);
    }
}
